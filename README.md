### 目的
本项目致力于生成海报图片，使用`html`转换`image`的方案实现，通过`Java`调取`PhantomJs`来进行转换。

### 环境准备

在使用本项目之前，需要安装`PhantomJs`到运行项目的服务器环境中，如下提供`Mac/Linux`两种方式的方式：
- `Mac方式下载`
```
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-macosx.zip
```
- `Linux方式下载`
```
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
```
下载完成后准备解压文件，解压完成后得到`bin`目录下的`phantomjs`可执行文件，我们需要把该文件放置到`/usr/local/bin`目录下：
```
cp phantomjs-2.1.1-macosx/bin/phantomjs /usr/local/bin
```

### 集成本项目依赖

在你的项目中添加`html-convert-image`的依赖，如下分两种集成方式：
- `Maven环境`
```
<dependency>
    <groupId>com.gitee.hengboy</groupId>
    <artifactId>html-convert-image</artifactId>
    <version>1.0.0.RELEASE</version>
</dependency>
```
- `Gradle环境`
```
compile group: 'com.gitee.hengboy', name: 'html-convert-image', version: '1.0.0.RELEASE'
```

### 怎么生成呢？
生成比较简单方便，只需要传递`html`的网址就可以了，如下所示：
```
// 执行html转换图片
String url = "https://cloud.tencent.com/developer";
BufferedImage img = Html2ImageByJsWrapper.renderHtml2Image(url);
// 获取图片base64字符串
String imageBase64 = HtmlToImageWrapper.getImageBase64(img,MediaType.ImagePng);
```
得到图片的`base64`字符串，你就可以通过工具类把`base64`转换成图片了。
